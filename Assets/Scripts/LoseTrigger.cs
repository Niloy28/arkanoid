﻿/************************************************************
 * Script that handles:
 *  - Lose condition, if the ball triggers the attached
 *    gameobject, call GameSession.DeductLife()
 ************************************************************/

using UnityEngine;

public class LoseTrigger : MonoBehaviour
{
    private GameSession gameSession;

    private void Awake()
    {
        gameSession = FindObjectOfType<GameSession>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(gameSession.DeductLife());
        }
    }
}