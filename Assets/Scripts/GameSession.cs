﻿/************************************************************
 * Script that handles:
 *  - Controls game speed
 *  - Updates score for each block broken
 *  - Adds one life for each level clear
 *  - Slows down ball and disables input on level clear
 *  - For each LoseTrigger enter
 *      - Deducts one life 
 *      - Triggers ball destroy vfx
 *      - Resets ball and paddle
 *  - Updates score and lives text accordingly
 ************************************************************/

using UnityEngine;
using TMPro;
using System.Collections;

public class GameSession : MonoBehaviour
{
    // configuration parameters
    [Range(0, 10)][SerializeField] private float gameSpeed = 1.0f;
    [Range(0, 1)] [SerializeField] private float reductionFactor = 0.2f;
    [SerializeField] private int pointsPerBlock = 25;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private int livesLeft = 3;
    [SerializeField] private bool isAutoPlayEnabled;
    [SerializeField] private float transitionTime = 1.5f;

    // game states
    private int currentScore = 0;

    // cached references
    private Ball ball;
    private Paddle paddle;

    // private methods
    private void Awake()
    {
        // duct tape fix to load references for the new scenes
        foreach (var session in FindObjectsOfType<GameSession>())
        {
            session.CacheReferences();
        }

        // implement singleton pattern so there is one instance of GameSession active.
        int gameSessionCount = FindObjectsOfType<GameSession>().Length;

        if (gameSessionCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            CacheReferences();
        }
    }

    private void Start()
    {
        UpdateScoreText();
        UpdateLivesText();
    }

    private void CacheReferences()
    {
        paddle = FindObjectOfType<Paddle>();
        ball = FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    private void Update()
    {
        // set game speed
        Time.timeScale = gameSpeed;
    }
	
    private void UpdateScoreText()
    {
        scoreText.text = "Current Score: " + currentScore.ToString();
    }

    private void UpdateLivesText()
    {
        livesText.text = "Lives Left: " + livesLeft.ToString();
    }

    // public methods
    public void AddToScore()
    {
        currentScore += pointsPerBlock;
        UpdateScoreText();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlayEnabled;
    }

    public void AddLife()
    {
        ++livesLeft;
        UpdateLivesText();
    }

    public IEnumerator DeductLife()
    {
        livesLeft--;
        ball.TriggerParticleVfx();
        yield return new WaitForSecondsRealtime(transitionTime);

        if (livesLeft < 0)
        {
            StartCoroutine(FindObjectOfType<SceneLoader>().LoadGameOver());
        }
        else
        {
            UpdateLivesText();
            paddle.ResetPaddle();
            ball.ResetBall();
        }
    }

    public void SlowMo()
    {
        paddle.DisableInput();
        ball.ReduceVelocity(reductionFactor);
    }

    public int GetScore()
    {
        return currentScore;
    }

    public float ReturnTransitionTime()
    {
        return transitionTime;
    }
}
