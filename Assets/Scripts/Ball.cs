﻿/************************************************************
 * Script that handles:
 *  - Ball collision
 *  - Ball position reset after triggering lose condition
 *  - Ball destroy vfx instantiation
 *  - Resets ball when lose condition occurs
 *  - Play random audio clip at collisions
 *  - Reduces ball velocity at level clears
 ************************************************************/

using UnityEngine;

public class Ball : MonoBehaviour
{
    // configuration parameters
    [SerializeField] private Vector2 startVelocity;
    [SerializeField] private AudioClip[] ballSfx;
    [SerializeField] private GameObject ballDestroyVfx;
    [SerializeField] private float rotationSpeed = 0.4f;
    [SerializeField] private float velocityTweakingFactor = 0.4f;

    // cached component references
    private Rigidbody2D rigidBody2D;
    private AudioSource audioSource;
    private Paddle paddle;

    // game states
    private bool isLaunched = false;
    private Vector2 startPos;

    private void Awake()
    {
        startPos = transform.position;
    }

    // Start is called before the first frame update
    private void Start()
    {
        CacheReferences();
    }

    private void CacheReferences()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        paddle = FindObjectOfType<Paddle>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (!isLaunched)
        {
            LockBallToPaddle();
            LaunchBallOnMouseClick();
        }
    }

    private void FixedUpdate()
    {
        RotateBall();
    }

    private void RotateBall()
    {
        rigidBody2D.rotation += rotationSpeed * 1f;

        if(rigidBody2D.rotation > 360)
        {
            rigidBody2D.rotation = 0;
        }
    }

    private void LaunchBallOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isLaunched = true;
            rigidBody2D.velocity = startVelocity;
        }
    }

    private void LockBallToPaddle()
    {
        transform.position = new Vector2(paddle.transform.position.x, transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isLaunched)
        {
            float xVelocityTweak = Random.Range(0.0f, velocityTweakingFactor);
            float yVelocityTweak = Random.Range(0.0f, velocityTweakingFactor);

            Vector2 velocityTweak = new Vector2(xVelocityTweak, yVelocityTweak);

            AudioClip audioClip = ballSfx[Random.Range(0, ballSfx.Length)];
            audioSource.PlayOneShot(audioClip);

            rigidBody2D.velocity += velocityTweak;
        }
    }

    public void ReduceVelocity(float reductionFactor)
    {
        rigidBody2D.velocity *= reductionFactor;
    }

    public void TriggerParticleVfx()
    {
        _ = Instantiate(ballDestroyVfx, new Vector2(transform.position.x, paddle.transform.position.y), Quaternion.identity);
    }

    public void ResetBall()
    {
        transform.position = startPos;
        rigidBody2D.velocity = Vector2.zero;

        isLaunched = false;
    }
}