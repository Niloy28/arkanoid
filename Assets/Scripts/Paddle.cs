﻿/************************************************************
 * Script that handles:
 *  - Paddle movement with mouse
 *  - Clamps paddle inside the game window
 *  - Resets paddle when lose condition occurs
 *  - Stops input at level clear
 ************************************************************/

using UnityEngine;

public class Paddle : MonoBehaviour
{
    [SerializeField] private float minPaddlePos;
    [SerializeField] private float maxPaddlePos;

    private float aspectRatio;
    private float screenWidthInUnits;

    // cached references
    private GameSession gameSession;

    // game states
    private Vector2 startPos;
    private bool isInputEnable = true;

    private void Awake()
    {
        startPos = transform.position;

        gameSession = FindObjectOfType<GameSession>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        aspectRatio = (float)Screen.width / Screen.height;
        screenWidthInUnits = 2 * aspectRatio * Camera.main.orthographicSize;
    }

    // Update is called once per frame
    private void Update()
    {
        if (isInputEnable)
        {
            MovePaddle();
        }
    }

    private void MovePaddle()
    {
        float paddleXPos = Mathf.Clamp(GetXPos(), minPaddlePos, maxPaddlePos);

        transform.position = new Vector2(paddleXPos, transform.position.y);
    }

    private float GetXPos()
    {
        if (gameSession.IsAutoPlayEnabled())
        {
            return FindObjectOfType<Ball>().transform.position.x;
        }
        else
        {
            return (Input.mousePosition.x / Screen.width) * screenWidthInUnits;
        }
    }

    public void DisableInput()
    {
        isInputEnable = false;
    }

    public void ResetPaddle()
    {
        transform.position = startPos;
    }
}