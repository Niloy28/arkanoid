﻿/************************************************************
 * Script that handles:
 *  - Gets score from GameSession before destroying
 *    the object
 *  - Displays score on GameOver scene
 ************************************************************/

using UnityEngine;
using TMPro;

public class DisplayScoreAtGameOver : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;

    private GameSession gameSession;
    
    private void Awake()
    {
        gameSession = FindObjectOfType<GameSession>();
    }

    // Start is called before the first frame update
    void Start()
    {
        string score = gameSession.GetScore().ToString();
        gameSession.ResetGame();

        scoreText.text = "Score: " + score;
    }
}
