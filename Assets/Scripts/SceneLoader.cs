﻿/************************************************************
 * Script that handles:
 *  - Shows cursor at Awake
 *  - Scene transition at level clear
 *      - Calls GameSession.SlowMo()
 *      - Retrieves transition time from GameSession and 
 *        waits for that period in real-time
 *      - Calls GameSession.AddLife() to give an extra life
 *        at each level clear transition
 *  - Scene transition at game over
 *      - Retrieves transition time from GameSession and 
 *        waits for that period in real-time
 *      - Loads GameOver scene
 *  - Scene transition at level select screen
 *      - Loads specific scene according to button pressed
 ************************************************************/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private GameSession gameSession;

    public void Awake()
    {
        gameSession = FindObjectOfType<GameSession>();
    }

    private void Start()
    {
        ShowCursor();
    }

    private void ShowCursor()
    {
        Cursor.visible = true;
    }

    public IEnumerator LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        // slow down movement and wait before transition
        gameSession.SlowMo();
        yield return new WaitForSecondsRealtime(gameSession.ReturnTransitionTime());
        SceneManager.LoadScene(currentSceneIndex + 1);

        // give player a life after completing a level
        gameSession.AddLife();
    }

    public void LoadSpecificScene(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();

        Debug.Log("Application closed");
    }

    public IEnumerator LoadGameOver()
    {
        yield return new WaitForSecondsRealtime(gameSession.ReturnTransitionTime());

        SceneManager.LoadScene("GameOver Screen");
    }
}
