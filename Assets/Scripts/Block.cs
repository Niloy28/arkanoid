﻿/************************************************************
 * Child class of AbstractBlock
 * 
 * Script that handles:
 *  - Implement HandleHit() for
 *      - Managing block health
 *      - Changing block sprite to show damage
 *      - Destroy block if block health is zero
 ************************************************************/

using UnityEngine;

public class Block : AbstractBlock
{
    // configuration parameters
    [SerializeField] private Sprite[] blockSprites;

    internal override void HandleHit()
    {
        int maxBlockHealth = blockSprites.Length;

        ++currentDamage;
        if (currentDamage >= maxBlockHealth)
        {
            DestroyBlock();
        }
        else
        {
            ShowBlockSprite();
        }
    }

    protected void ShowBlockSprite()
    {
        int spriteIndex = currentDamage < blockSprites.Length ? currentDamage : blockSprites.Length - 1;

        if (blockSprites[spriteIndex] != null)
        {
            spriteRenderer.sprite = blockSprites[spriteIndex];
        }
        else
        {
            Debug.LogError(gameObject.name + "'s blockSprite array is missing data.");
        }
    }
}