﻿/************************************************************
 * Base class from which Block and BossBlock is derived.
 * Implements common methods and attributes.
 * 
 * Script that handles:
 *  - Block counting through calling LevelControl.CountBreakableBlocks()
 *    when the block is tagged as "Breakable"
 *  - Handles collision between ball and breakable blocks
 *  - Destroys block when conditions are satisfied
 *  - Plays block destroy audio clip
 *  - Plays block destroy particle vfx
 ************************************************************/

using UnityEngine;

public abstract class AbstractBlock : MonoBehaviour
{
    // configuration parameters
    [SerializeField] protected AudioClip destroyClip;
    [SerializeField] protected GameObject impactVfx;

    // cached references
    protected SpriteRenderer spriteRenderer;
    protected LevelControl levelControl;

    // game states
    [SerializeField] protected int currentDamage;

    protected virtual void Awake()
    {
        if (CompareTag("Breakable"))
        {
            CacheReferences();
        }
    }

    protected virtual void Start()
    {
        CountBreakableBlocks();
    }

    protected virtual void CacheReferences()
    {
        levelControl = FindObjectOfType<LevelControl>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void CountBreakableBlocks()
    {
        if (CompareTag("Breakable"))
        {
            levelControl.CountBlocks();
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && CompareTag("Breakable"))
        {
            HandleHit();
        }
    }

    internal abstract void HandleHit();

    protected void DestroyBlock()
    {
        AudioSource.PlayClipAtPoint(destroyClip, Camera.main.transform.position);

        levelControl.OnBlockDestroy();

        TriggerParticleVfx();
        Destroy(gameObject);
    }

    protected void TriggerParticleVfx()
    {
        _ = Instantiate(impactVfx, transform.position, Quaternion.identity);
    }
}
