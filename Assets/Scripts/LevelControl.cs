﻿/************************************************************
 * Script that handles:
 *  - Hides mouse cursor in window during gameplay
 *  - Counts breakable blocks
 *  - Updates score through GameSession for each block
 *    broken, loads next level when all blocks destroyed
 ************************************************************/

using UnityEngine;

public class LevelControl : MonoBehaviour
{
    // game parameters
    [SerializeField] private int breakableBlocks;

    // cached references
    private SceneLoader sceneLoader;
    private GameSession gameSession;

    private void Start()
    {
        HideCursor();
        CacheReferences();
    }

    private void HideCursor()
    {
        Cursor.visible = false;
    }

    private void CacheReferences()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
        gameSession = FindObjectOfType<GameSession>();
    }

    public void CountBlocks()
    {
        ++breakableBlocks;
    }

    public void OnBlockDestroy()
    {
        --breakableBlocks;
        gameSession.AddToScore();

        if (breakableBlocks == 0)
        {
            StartCoroutine(sceneLoader.LoadNextScene());
        }
    }
}
