﻿/************************************************************
 * Child class of AbstractBlock
 * 
 * Script that handles:
 *  - Boss Block movement
 *  - Implement HandleHit() for
 *      - Managing block health
 *      - Changing block sprite color to show damage
 *      - Destroy block if block health is zero
 ************************************************************/

using UnityEngine;

public class BossBlock : AbstractBlock
{
    private enum Direction
    {
        Left = -1,
        Right = 1
    }

    [SerializeField] private float minBlockPos;
    [SerializeField] private float maxBlockPos;
    [SerializeField] private Vector2 startVelocity;
    [SerializeField] private int bossBlockMaxHealth = 5;

    // starting direction
    private Direction direction;

    protected override void Start()
    {
        direction = Direction.Right;
        base.Start();
    }

    private void Update()
    {
        MoveBossBlock();
    }

    private void MoveBossBlock()
    {
        Vector2 currentPos = transform.position;

        if (currentPos.x <= minBlockPos)
        {
            direction = Direction.Right;
        }
        else if (currentPos.x >= maxBlockPos)
        {
            direction = Direction.Left;
        }

        transform.position = new Vector2(transform.position.x + ((int)direction * startVelocity.x), transform.position.y);
    }

    internal override void HandleHit()
    {
        ++currentDamage;
        if (currentDamage >= bossBlockMaxHealth)
        {
            DestroyBlock();
        }
        else
        {
            UpdateBossBlockColor();
        }
    }

    private void UpdateBossBlockColor()
    {
        float saturationChangePerHit = 1.0f / bossBlockMaxHealth;

        // get current block color values
        Color currBlockColor = spriteRenderer.color;
        Color.RGBToHSV(currBlockColor, out float currBlockHue, out float currBlockSat, out float currBlockV);

        // update color saturation values
        spriteRenderer.color = Color.HSVToRGB(currBlockHue, currBlockSat + saturationChangePerHit, currBlockV);
    }
}
